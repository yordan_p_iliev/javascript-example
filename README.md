This project has a login page and a main page. The main page can be only seen by a logged in user. I used eval() because there is no back end to return the proper main page.

Valid users are admin,123; server,abc; istrator,abv.

The app displays users from randomuser.me in a table. It is responsive.

When opening a new page it fetches the users while using the logged-in user's name as a seed and stores them. When opening the page again it fetches them directly from its cache.

Clicking on a user from the table shows more information.